Inscription à GitLab
======================

Ce tutoriel a pour objectif d'explique la manière de créer un utilisateur sur 
[GitLab.com](https://gitlab.com/)

Il faut se retrouver sur la page "Sign in" de GitLab, c'est-à-dire la page permettant soit de 
s'authentifier soit de s'inscrire. On peut passer par la page d'accueil de GitLab

![1_gitlab_homepage](https://gitlab.com/DREES/tutoriels/raw/master/img/inscription/1_gitlab_homepage.PNG)

Ou en cliquant sur un lien issu d'un email d'invitation.

On se retrouve sur la page suivante:

![2_Onglet_Sign_in](https://gitlab.com/DREES/tutoriels/raw/master/img/inscription/2_onglet_sign_in.PNG)

Pour les utilisateurs déjà inscrits il suffit d'indiquer son nom ou email et son mot de passe. Pour un 
nouvel utilisateur il faut cliquer sur l'onglet "Register" qui permet de commencer son inscription:

![3_Onglet_register](https://gitlab.com/DREES/tutoriels/raw/master/img/inscription/3_onglet_register.PNG)

Pour s'inscrire il faut remplir l'ensemble du formulaire avant de confirmer. Il est vivement conseillé 
d'utiliser son email professionnel type @sante.gouv.fr pour cette inscription.

Par la suite un email de confirmation est envoyé sur votre boîte mail:

![4_email_de_confirmation_outlook](https://gitlab.com/DREES/tutoriels/raw/master/img/inscription//4_email_de_confirmation_outlook.PNG)

ou

![5_email_de_confirmation_gmail](https://gitlab.com/DREES/tutoriels/raw/master/img/inscription/5_email_de_confirmation_gmail.PNG)

Il suffit de confirmer et de s'authentifier pour pouvoir avoir accès aux différents projets.

![6_fin_d_inscription](https://gitlab.com/DREES/tutoriels/raw/master/img/inscription/6_fin_d_inscription.PNG)


[Tutoriel d'utilisation de GitLab](https://gitlab.com/DREES/tutoriels/wikis/tutoriel-gitlab)

